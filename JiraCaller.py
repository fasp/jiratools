#!/usr/bin/python

import os, requests, getpass, logging
from collections import defaultdict
from datetime import datetime, timedelta
logging.basicConfig(format='%(message)s')
baseURL=''
issue_hours={}  # TODO make dict

ITAM_ID='customfield_10002'

#weeklog=defaultdict(lambda: [None,None,None,None,None,None,None]) #{[] for i in range(10)}
weeklog=defaultdict(lambda: [[],[],[],[],[],[],[]]) 

class Issue(object):
    _issueID=None
    _log=None
    _summary=None
    _auftrag_id=None

    def log(self):
        return self._log

    def summary(self):
        return self._summary
    
    def auftragID(self):
        return self._auftrag_id
        
    def __init__(self, issueID, auftrag_id, to_be_logged, summary):
        self._issueID=issueID
        self._auftrag_id=auftrag_id
        self._log=to_be_logged
        self._summary=summary
        
def get_issues_by_user(username):    
    request='search?jql=assignee='+username
    r = requests.get(baseURL + request, auth=(username, password))
    if r.status_code==200:
        content=r.json()
        for issue in content['issues']:
            #__log_issue_details(issue)
            __get_details_for_issue(issue)
    elif r.status_code==401:
        logging.error('Falsches Passwort oder Benutzer')
    else:
        logging.error('Ein Fehler ist aufgetreten. Schade.')

def __log_issue_details(issue):
    progress=issue['fields']['progress']
    hours_spent=progress['progress']/60**2
    estimated=progress['total']/60**2
    percent=progress['percent'] if 'percent' in progress else 0        
    print(issue['key']+': '+ issue['fields']['summary'], '\n\tGearbeitet '+str(round(hours_spent, 2))+' von '+str(round(estimated, 2))+' ('+str(percent)+'%)')
        
def __get_details_for_issue(issue):
    issueID=issue['key']
    summary=issue['fields']['summary']
    itam_id=issue['fields'][ITAM_ID]
    request='issue/'+issueID+'/worklog'
    r = requests.get(baseURL + request, auth=(username, password))
    content=r.json()
    for log in content['worklogs']:
        #logdate=(datetime.strptime(log['started'][:10], '%Y-%m-%d')).date()
        logdate=(datetime.strptime(log['started'][:10], '%Y-%m-%d'))
        if __date_is_in_current_week(logdate):
            weekday=logdate.weekday()
            weeklog[issueID][weekday].append(log)
            already_logged=issue_hours[issueID].log() if issueID in issue_hours else 0
            new_log=log['timeSpentSeconds']/60**2
            to_be_logged=already_logged+new_log
            issue_hours[issueID]= Issue(issueID, itam_id, to_be_logged, summary)

def __date_is_in_current_week(date):
    today=datetime.now().date()
    start = today - timedelta(days=today.weekday())
    end = start + timedelta(days=6)
    return start<=date.date()<=end

def print_weeklog():
    print(3*'\n'+'\t\tMo\tDi\tMi\tDo\tFr\tSa\tSo')
    for issue_id in weeklog:
        issue_week_log=''
        issue_week_log+=issue_id
        for day in range(7):
            issue_logs_at_day=weeklog[issue_id][day]
            hours=0
            if issue_logs_at_day:
                for log in issue_logs_at_day:
                    logged_time=log['timeSpentSeconds']/60**2
                    hours+=logged_time
            issue_week_log+='\t'+str(hours)
        print(issue_week_log)
    
password=getpass.getpass('Password:')
s=requests.Session()
username=os.getlogin()
s.auth=(username, password)
get_issues_by_user(username)
print_weeklog();
# for issue in issue_hours:
#     tmp_issue=issue_hours[issue]
#    print('Auftrag '+tmp_issue.auftragID()+' ('+issue+'): '+str(issue_hours[issue].log())+' Stunden\n\t'+issue_hours[issue].summary()+'\n')